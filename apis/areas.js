const express = require('express')
var router = express.Router()
const mongodb = require('mongodb');
const ObjectID = require('mongodb').ObjectID;

const DB_NAME = 'ticket_monster'
const COLLECTION_NAME = 'areas'

const DB_URI = 'mongodb://localhost:27017';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

router.get('/', function(req, res) {
  client.connect(function(err, connection) {
    const db = connection.db(DB_NAME); //connecting to the databse
    db.collection(COLLECTION_NAME).find({}).toArray(function (find_err, records)  {
      if(find_err)
        return res.status(500).send(find_err)
      res.send(records);
    });
  });
});

router.post('/', function(req, res) {
  if(!req.body || req.body.length === 0) //if it is null, it means they didnt send data
    return res.status(400).send({message: "Record is required."});

  if(!req.body.type || !req.body.name || !req.body.rank  || !req.body.areatype)
    return res.status(400).send({message: "Fix your data. The following data keys are required: type, name, rank, areatype"});

  client.connect(function(err, connection) {
    const db = connection.db(DB_NAME); //connecting to the database

    db.collection(COLLECTION_NAME).insertOne(req.body, function(insert_error, data){
      if(insert_error)
        return res.status(500).send({message: "Something went wrong."}); //HTTP status code page

      connection.close()
      return res.status(200).send({message: "Your item " + req.body._id + " has been inserted successfully into " + COLLECTION_NAME}) //if it actually works (thats what 200 is)
    });
  })
})

// Put request
//if I add more after /:id, like /:id/:name, itll parse data into id and name
router.put('/:id', function(req, res){
    client.connect(function(err, connection) {
      if(err)
        return res.status(500).send({error: err}) //because it was a server error, we use 500

      if(!req.body || req.body.length === 0) //if it is null, it means they didnt send data
        return res.status(400).send({message: "Records are required."});

      const db = connection.db(DB_NAME)
      db.collection(COLLECTION_NAME)
        .updateOne({_id: ObjectID(req.params.id)},{$set: req.body}, function(update_err, update_data){
            if(update_err)
              return res.status(500).send({error: "Couldn't update your records"}) //because it was a server error, we use 500
            return res.status(200).send({error: "The update for " + req.body.name + " was successful!", data: update_data})
        })
    })
})

router.delete('/:id', function(req, res){
  client.connect(function(err, connection) {
    if(err)
      return res.status(500).send({error: err}) //because it was a server error, we use 500

      const db = connection.db(DB_NAME)
      db.collection(COLLECTION_NAME).deleteOne(req.body, function(err, data){
        if(err)
          return res.status(500).send({error: "The removal was unsuccessful.", data: date})

        return res.status(200).send({error: "This item " + req.body.name + " has been deleted", data: data})
      })
  })

})

module.exports = router;
