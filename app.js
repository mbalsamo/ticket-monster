const express = require('express');
const mongodb = require('mongodb');
const bodyParser = require('body-parser');
const ObjectID = require('mongodb').ObjectID;

const areas = require('./apis/areas');
const products = require('./apis/products')
const offers = require('./apis/offers')
const passwords = require('./apis/passwords')
const pricezones = require('./apis/pricezones')
const events = require('./apis/events')
const prices = require('./apis/prices')
const venues = require('./apis/venues')
const segments = require('./apis/segments')
const genres = require('./apis/genres')
const subgenres = require('./apis/subgenres')
const images = require('./apis/images')
const attractions = require('./apis/attractions')

const app = express();
const PORT = 5050;


app.use(bodyParser.urlencoded({extended: false})); //allow user to send data within URL
app.use(bodyParser.json()); //allow user to send JSON data

//add this for each one of the things
app.use('/areas', areas);
app.use('/products', products)
app.use('/offers', offers)
app.use('/passwords', passwords)
app.use('/pricezones', pricezones)
app.use('/events', events)
app.use('/prices', prices)
app.use('/venues', venues)
app.use('/segments', segments)
app.use('/genres', genres)
app.use('/subgenres', subgenres)
app.use('/images', images)
app.use('/attractions', attractions)

app.listen(PORT);
console.log("Listening on port " + PORT);
